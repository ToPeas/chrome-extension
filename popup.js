// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

'use strict';

let changeColor = document.getElementById('changeColor');

chrome.storage.sync.get('color', function (data) {
  changeColor.style.backgroundColor = data.color;
  changeColor.setAttribute('value', data.color);
});

changeColor.onclick = function (element) {
  let color = element.target.value;
  var xhr = new XMLHttpRequest();
  xhr.open("GET", "https://cnodejs.org/api/v1/topics", true);
  xhr.onreadystatechange = function () {
    if (xhr.readyState == 4) {
      // 警告！可能会执行恶意脚本！
      // var resp = eval("(" + xhr.responseText + ")");
      console.log(xhr.responseText)

    }
  }
  xhr.send();
  chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
    chrome.tabs.executeScript(
      tabs[0].id,
      { code: 'document.body.style.backgroundColor = "' + color + '";' });
  });
};

chrome.windows.getCurrent(function(currentWindow)
{
	console.log('current window：' + currentWindow.id);
});
